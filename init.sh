#!/bin/bash

if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

USER=$GITLAB_USER;
PASS=$GITLAB_PASSWORD;
HOST=$GITLAB_HOST;
R_HOST=$REGISTRY_HOST;
R_USER=$REGISTRY_USER;
R_PASSWORD=$REGISTRY_PASSWORD;

echo "AUTHENTICATING INTO GILAB SERVER..``";
echo "grant_type=password&username=$USER&password=$PASS" > auth.txt
access_token=$(curl --data "@auth.txt" --request POST http://gitlab.jalasoft.org/oauth/token 2>/dev/null | python -c 'import  json,sys; result=json.load(sys.stdin); print(result["'access_token'"])')

echo "AUTHENTICATING INTO REGISTRY SERVER..``";
docker login -u $R_USER -p $R_PASSWORD $R_HOST  2>/dev/null

get_last_migrations () {
    PROJECT_ID=$1;
    MIGRATION_LOCATION=$2;
    FILES_FOLDER=$3;
    MATCH_FOLDER=$4;
    FILE_POS=6;

    if [ -n "$(ls -A $MIGRATION_LOCATION 2>/dev/null)" ]
    then
        rm -r $MIGRATION_LOCATION/*;
    fi

    # Download the repository
    curl --header "Authorization: Bearer $access_token" "http://gitlab.jalasoft.org/api/v4/projects/$PROJECT_ID/repository/archive?sha=dev" --output $PROJECT_ID.tar.gz 2>/dev/null
    # Get the file directory
    SCRIPT_DIR=$(tar tvf $PROJECT_ID.tar.gz 2>/dev/null | awk "/^d.+\/${MATCH_FOLDER}\/$/{ print \$${FILE_POS} }");
    # Get the depth scripts directory
    NUMBER=$(echo $SCRIPT_DIR | tr -cd "/" | wc -c)
    NUMBER=$(($NUMBER - 1))
    # Untar the scripts with and save the files into a location
    tar zxvf $PROJECT_ID.tar.gz --strip $NUMBER $SCRIPT_DIR > /dev/null 2>&1 && mv $FILES_FOLDER/* $MIGRATION_LOCATION;

    # Remove the files
    rm $PROJECT_ID.tar.gz;
    rm -r $FILES_FOLDER;
}

get_image_from_docker() {
    PROJECT_ID=$1;
    IMAGE=$2;
    TAG=$(curl --header "Authorization: Bearer $access_token" "http://gitlab.jalasoft.org/api/v4/projects/$PROJECT_ID/pipelines?ref=dev&status=success&scope=finished" 2>/dev/null | python -c 'import  json,sys; result=json.load(sys.stdin); print(result[0]["'id'"])')

    FULL_IMAGE=$R_HOST/$IMAGE:$TAG;
    echo $FULL_IMAGE;
}

# Migrate the databases from scylladb and postgres
echo "Getting lasted migrations files for evaluations ....";
get_last_migrations 260 "cassandra-migrate/migrations/scripts" "scripts" "migrations\/scripts";


echo "Getting lasted migrations files for users ....";
get_last_migrations 282 "postgres-migrate/migrations" "migrations" "migrations";

echo "Running the docker compose file";
# Remote trash file
rm cassandra-migrate/migrations/scripts/V000__createKeyspace.cql 2>/dev/null

# Remote auth file
rm auth.txt


# Start the service
echo "STARTING THE DATABASES AND MAKING THE MIGRATIONS";
docker-compose up -d

if [ "$1" = "-e" ]
then
    echo "STARTING USERS SERVICE";
    EVALUATION_IMAGE=$(get_image_from_docker 260 repository/evapro-docker-private/backend-evaluations)
    sed -e "s|E_IMAGE|${EVALUATION_IMAGE}|g" conf/services.yaml > docker-compose-service.yaml
    docker-compose -f docker-compose-service.yaml up evaluations -d
fi

if [ "$1" = "-eu" ]
then
    echo "STARTING ALL SERVICES";
    EVALUATION_IMAGE=$(get_image_from_docker 260 repository/evapro-docker-private/backend-evaluations)
    USERS_IMAGE=$(get_image_from_docker 282 repository/evapro-docker-private/backend-users)
    sed -e "s|U_IMAGE|${USERS_IMAGE}|g" conf/services.yaml | sed  "s|E_IMAGE|${EVALUATION_IMAGE}|g" > docker-compose-service.yaml
    docker-compose -f docker-compose-service.yaml up -d
fi

if [ "$1" = "-u" ]
then
    echo "STARTING USERS SERVICEA";
    USERS_IMAGE=$(get_image_from_docker 282 repository/evapro-docker-private/backend-users)
    sed -e "s|U_IMAGE|${USERS_IMAGE}|g" conf/services.yaml > docker-compose-service.yaml
    docker-compose -f docker-compose-service.yaml up users -d
fi


