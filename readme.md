# EVAPro Setup Development enviroment

## Configuration is on windows (default)

### Requirements

* Docker
* dotnet-2.1
* docker-compose

For Linux environments uncomment the following lines and comment on the windows ones in the file `nginx/nginx.conf`

```bash
server 172.17.0.1:8087;
server 172.17.0.1:8088;
```

### Run EVA environment

```bash
docker-compose up -d
```

### Mongo Express

```bash
http://localhost:8081/
```
