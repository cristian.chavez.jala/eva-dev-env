#!/bin/bash


if [ "$1" = "-r" ]
then
    echo "STOPING THE SERVICES";
    docker-compose rm

    if test -f "docker-compose-service.yaml"; then
        docker-compose  -f docker-compose-service.yaml rm
    fi
else
    echo "STOPING THE SERVICES";
    docker-compose stop

    if test -f "docker-compose-service.yaml"; then
        docker-compose  -f docker-compose-service.yaml stop
    fi
fi
